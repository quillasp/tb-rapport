\startchapter[title={Implémentation}]
Dans ce chapitre, nous détaillerons les étapes définies dans la conception pour
mettre en place la gestion des tuiles raster. Petit rappel sur les grands points
à modifier:\blank[line]

\startdomain
\item les requêtes pour obtenir les tuiles rasters,
\item traitement de la tuile,
\item ajouter la tuile dans le dépôt de tuiles,
\item effectuer le rendu des tuiles.
\stopdomain

Commençons par détailler ces différentes parties.

\startsection[title={Requêtes pour obtenir les tuiles rasters}]
Dans la partie Conception, nous avons défini ce qu'il fallait faire pour pouvoir
faire des requêtes à un autre service selon le type de tuile que nous voulons
récupérer. Mais avant de commencer à faire ceci, il faut que nous ajoutions une
manière de pouvoir représenter les tuiles raster comme une nouvelle couche à
l'intérieur de maplibre-rs.

\startsubsection[title={Ajout de la couche}]
Nous allons commencer par ajouter une représentation d'une tuile raster en
suivant les spécifications de Mapbox\footnote{Que nous retrouvons ici
\from[raster-style-spec]} en utilisant une structure Rust en suivant ce qui a
déjà été fait pour les autres couches. Voici ce que cela nous
donne:

\startRUST
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RasterLayer {
    #[serde(rename = "raster-brightness-max")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub raster_brightness_max: Option<f32>,
    #[serde(rename = "raster-brightness-min")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub raster_brightness_min: Option<f32>,
    #[serde(rename = "raster-contrast")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub raster_contrast: Option<f32>,
    #[serde(rename = "raster-fade-duration")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub raster_fade_duration: Option<u32>,
    #[serde(rename = "raster-hue-rotate")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub raster_hue_rotate: Option<f32>,
    #[serde(rename = "raster-opacity")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub raster_opacity: Option<f32>,
    #[serde(rename = "raster-resampling")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub raster_resampling: Option<RasterResampling>,
\stopRUST

\page

\startRUST
    #[serde(rename = "raster-saturation")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub raster_saturation: Option<f32>,
    // pub visibility: Option<RasterVisibility>,
}
\stopRUST


Il faudra aussi rajouter une représentation d'un élément {\tt Raster} dans un
{\emph enum} représentant les différents types de « peinture »\footnote{Mapbox
parle des différentes couches à afficher comme {\tt Paint}, d'où la peinture.}.
Il s'y trouve déjà les éléments {\tt Background}, {\tt Line} et {\tt Fill}. Et
pour terminer cette partie, nous aurons à ajouter la structure décrite
précédemment dans la structure décrivant les styles, expliquant entre autre le
type d'élément à représenter, s'il s'agit d'un parc, comment le peindre, sa
couleur, etc.

\stopsubsection

\startsubsection[title={Les types de couches}]
Un autre endroit où nous devrons opérer une modification est dans le dépôt de
tuiles (ou {\tt TileRepository}). Il s'agit de la méthode {\tt
is_layers_missing()} qui va parcourir les couches se trouvant à une coordonnée
pour indiquer si oui ou non une couche est manquante. Initialement, {\tt
RequestStage} utilise cette méthode pour déterminer quelle couche d'une tuile
doit encore faire l'objet d'une requête.

Nous changerons cette méthode pour qu'elle retourne un {\tt HashSet<&str>} à la
place d'un booléen, ceci afin qu'une requête différente soit faite s'il s'agit
de la couche raster ou d'une autre couche.

Nous devrons aussi ajouter une définition pour la couche raster {\tt
RasterLayer} dans l'{\emph enum} {\tt StoredLayer} toujours dans le {\tt
TileRepository}. Un autre {\emph enum} lié est celui de {\tt
LayerTessellateMessage} qui sera utilisé par {\tt SharedThreadState} pour
envoyer les données une fois qu'elles ont été traitées. Ces deux énumérations
seront identiques conformément à ce qui est déjà mis en place.
\stopsubsection

\startsubsection[title={Types de source}]
Maplibre-rs peut déjà faire des requêtes à un service pour obtenir des tuiles
vectorielles. Dans la conception, nous avons indiqué que c'est {\tt
SourceClient} qui fait la requête. Il reçoit les coordonnées {\emph x}, {\emph
y} et {\emph z} de {\tt RequestStage} et va former une URL à partir de ces
paramètres. Voici à quoi cela ressemble:

\startRUST
fetch(&self, coords: &WorldTileCoords) -> Result<Vec<u8>, Error> {
    let tile_coords = coords.into_tile(TileAddressingScheme::TMS).unwrap();
    self.inner_client.fetch(
        format!(
            "https://maps.tuerantuer.org/europe_germany/{z}/{x}/{y}.pbf",
            z = tile_coords.z,
            x = tile_coords.x,
            y = tile_coords.y,
        ).as_str()
    )
}
\stopRUST

Nous voulons donc que deux structures supplémentaires se chargent de construire
les URL pour leur type de tuile.

\stopsubsection

\startsubsection[title={Abstraction de la source}]
Pour que les deux nouvelles structures {\tt RasterSource} et {\tt
TessellateSource} puissent avoir leur propre service, nous aurons à créer un
{\emph trait} {\tt Source} qui possédera une seule méthode {\tt load()}. En
suivant les indications pendant le développement, cette méthode se retrouvera
avec cinq paramètres pour pouvoir fonctionner correctement au sein de maplibre-rs.

\startRUST
pub trait Source<SM, HC>
where
    SM: ScheduleMethod,
    HC: HttpClient,
{
    fn load(
        &self,
        http_source_client: HttpSourceClient<HC>,
        scheduler: &Scheduler<SM>,
        state: SharedThreadState,
        coords: &WorldTileCoords,
        request_id: TileRequestID,
    );
}
\stopRUST

\blank[line]

\startdomain
\item {\tt HttpSourceClient} pour effectuer la requête HTTP avec l'URL qui sera formée,
\item {\tt Scheduler} pour planifier la requête,
\item {\tt SharedThreadState} pour permettre le traitement des tuiles obtenues,
\item {\tt WorldTileCoords} pour les coordonnées de la tuile,
\item {\tt TileRequestID} pour identifier la requête.
\stopdomain

Les deux structures associées au type de tuile implémenteront chacun cette
méthode.

En plus de cela, dans un souci d'éviter la duplication de code, les deux
structures effectuant finalement les deux la même chose, à savoir lancer le
traitement une fois les données obtenues, nous allons créer un {\emph enum} {\tt
SourceType} qui regroupera ces deux structures. Et nous aurons à implémenter la
méthode {\tt load()} pour cet {\emph enum}.

\startRUST
impl<SM, HC> Source<SM, HC> for SourceType
where
    SM: ScheduleMethod,
    HC: HttpClient,
{
    fn load(
        &self,
        http_source_client: HttpSourceClient<HC>,
        scheduler: &Scheduler<SM>,
        state: SharedThreadState,
        coords: &WorldTileCoords,
        request_id: TileRequestID,
    ) {
        let client = SourceClient::Http(http_source_client.clone());
        let coords = *coords;
        let source = self.clone();
                …
                    match client.fetch(&coords, &source).await {
                        Ok(data) => match source {
                            SourceType::Raster(raster_source) => {
                                state
                                    .process_raster_data(request_id, data.into_boxed_slice())
                                    .unwrap();
                            }
                            SourceType::Tessellate(tessellate_source) => {
                                state
                                    .process_vector_data(request_id, data.into_boxed_slice())
                                    .unwrap();
                            }
                        },
                        Err(e) => {
                            log::error!("{:?}", e);
                            state.tile_unavailable(&coords, request_id).unwrap();
                        }
                    }
                …
    }
}
\stopRUST

Grâce à ceci, nous aurons les deux structures qui effectuerons un appel à la
méthode de l'{\emph enum} et il utilisera un pattern matching pour faire
correspondre la bonne méthode de traitement pour le type de tuile.

\stopsubsection

\stopsection

\startsection[title={Traitement de la tuile}]
C'est {\tt SharedThreadState} qui possède les méthodes pour traiter les tuiles.
Nous allons en ajouter une nouvelle {\tt process_raster_data} qui créera le
pipeline de traitement en s'inspirant ce que ce qui a déjà été fait pour les
tuiles vectorielles. D'ailleurs, cette méthode sera renommée {\tt
process_vector_data}.

Nous avions dit qu'on allait découpler le processus de traitement et de dépôt en
deux processus pour pouvoir y brancher le traitement de l'autre type de tuile.
Voici ce que cela donne pour la tuile vectorielle:

\startRUST
pub fn build_vector_tile_pipeline()
    -> impl Processable<Input = <ParseTile as Processable>::Input> 
{
    DataPipeline::new(
        ParseTile,
        DataPipeline::new(
            TessellateLayer,
            DataPipeline::new(TilePipeline, PipelineEnd::default()),
        ),
    )
}
\stopRUST

Avant, {\tt TessellateLayer} et {\tt TilePipeline} étaient une seule étape. Et
nous pouvons mettre en place le pipeline pour la tuile raster:

\page

\startRUST
pub fn build_raster_tile_pipeline()
    -> impl Processable<Input = <RasterLayer as Processable>::Input>
{
    DataPipeline::new(
        RasterLayer,
        DataPipeline::new(TilePipeline, PipelineEnd::default()),
    )
}
\stopRUST

Nous pouvons voir que {\tt TilePipeline} est l'étape de fin pour les deux types
de tuiles.

Chaque étape de traitement envoie les données qu'il a traitées à l'étape
suivante. Cela veut dire qu'avant la modification, le pipeline d'une tuile
vectorielle était le suivant:

\placefigure
  [fig:tile_pipeline]
  {Types entre les étapes du pipeline de traitement de la tuile vectorielle}
  {
    \externalfigure[tile_pipeline.svg][width=0.9\textwidth]
  }

Pour éviter un conflit de type entre les étapes, nous devons à nouveau faire un
{\emph enum} qui pourra contenir les deux types de tuiles que l'on appellera
{\tt TileType}.

\startRUST
pub enum TileType {
    Vector(geozero::mvt::Tile),
    Raster(Vec<u8>),
}
\stopRUST

Dans le schéma suivant, nous verrons ce que donne le nouveau pipeline de
traitement:

\placefigure
  [fig:tile_pipeline_after]
  {Types entre les étapes du pipeline de traitement des tuiles}
  {
    \externalfigure[tile_pipeline_after.svg][width=0.9\textwidth]
  }

\startsubsection[title={Traitement de la tuile raster}]
Pour le moment, le traitement de la tuile ne nécessite pas beaucoup
d'opérations. Nous avons juste à transformer le tableau de bytes en un vecteur
de bytes, qui est une version « possédée » (owned) de ce type de données pour
faciliter la gestion à cette étape du projet. Bien entendu, suivant le type de
raster que nous voulons obtenir, il est tout à fait envisageable de faire un
traitement plus avancé, suivant le type de données raster que nous avons
obtenues.
\stopsubsection

\startsubsection[title={Chargement de la tuile}]
Et pour terminer l'étape de traitement, nous devons encore ajouter une méthode
{\tt raster_layer_finished} qui se chargera d'envoyer les données traitées via
{\tt SharedThreadState} au {\tt TileRepository}.

La communication se passe de manière asynchrone en utilisant un canal {\tt mpsc}
dit « Multi-producer, single-consumer » permettant au {\tt TileRepository} de
recevoir plusieurs tuiles traitées par plusieurs processus effectuant les
traitements en concurrence. Ceci explique pourquoi les structures {\tt
StoredLayer} et {\tt LayerTessellateMessage} possèdent les mêmes entrées pour
simplifier le transfert de données.

\stopsubsection

\stopsection

\startsection[title={Rendu de la tuile}]
C'est la plus partie la plus technique du travail, car en plus d'avoir à mettre en
place les ressources nécessaires pour le rendu, il faut pouvoir les charger dans
le GPU, écrire les shaders qui utilisent un autre langage (WGSL), déterminer les
quatres sommets où l'on veut dessiner la tuile, etc. C'est la tâche qui a
demandé le plus de travail à ce jour.

Pour simplifier l'organisation du code, nous allons commencer par définir une
structure qui contiendra les ressources nécessaires au rendu:

\startRUST
pub struct RasterResources {
    pub sampler: Option<wgpu::Sampler>,
    pub msaa: Option<Msaa>,
    pub texture: Option<Texture>,
    pub raster_pipeline: Option<wgpu::RenderPipeline>,
    pub raster_bind_group: Option<wgpu::BindGroup>,
}
\stopRUST

Cette structure possédera aussi les méthodes nécessaires à l'initialisation de
chacun de ses attributs. Suite de quoi, il va falloir faire des changements dans
les étapes désignées dans la conception.

\startsubsection[title={Initialisation des ressources}]
C'est dans {\tt ResourceStage} que le travail commence. Ici les différentes
ressources sont initialisées, et nous allons faire de même pour les ressources de
la tuile raster. À cette étape, c'est le pipeline de rendu qui est préparé.
Voici ce que nous avons pour une tuile raster:

\startRUST
let tile_vertex = shaders::ShaderTextureVertex::default();
let tile_fragment = shaders::RasterTileShader {
    format: settings.texture_format,
};

let mut raster_resources = RasterResources::default();
raster_resources.set_sampler(device);
raster_resources.set_msaa(Msaa { samples: 1 });
raster_resources.set_raster_pipeline(device, &settings, &tile_vertex, &tile_fragment);
\stopRUST

{\tt tile_vertex} sera nécessaire pour définir les quatre sommets de la tuile.
Cette structure aura son propre shader, appelé le vertex shader. Il en aura
besoin pour savoir où dessiner sur la surface de rendu. Il lui faudra aussi les
coordonnées de la texture, car comme nous l'avons vu, ce sont deux systèmes de
coordonnées différents.

{\tt tile_fragment} aura un fragment shader qui sera responsable de la couleur à
donner à chaque « fragment » de l'objet que l'on veut dessiner. Elle est
représentée par {\tt vec4<f32>} où chaque élément détermine les canaux R, G, B
et A.

Ces deux éléments sont obligatoires pour pouvoir créer le pipeline de rendu.

Nous devons aussi modifier la structure responsable de créer le pipeline en
incluant deux {\tt wgpu::BindGroupLayoutEntry}, un pour la texture samplée et
l'autre pour le sampler.

\stopsubsection

\startsubsection[title={Chargement des ressources}]
Dans {\tt UploadStage}, nous allons charger les ressources initialisées à
l'étape d'avant. Pour reprendre ce qui se passe pour l'allocation des buffers
GPU pour les tuiles vectorielles, nous allons aussi allouer un buffer pour les
sommets (vertex buffer).

Puis nous allons créer la texture à partir des données qui se trouvent dans le
dépôt de tuiles, l'enregistrer dans {\tt RasterResources} et puis l'écrire dans
la queue du GPU. Et pour terminer l'étape de préparation, nous allons créer le
bind group qui est un ensemble de ressources accessibles par les shaders. Dans
ce cas-ci, seul le fragment shader pourra y avoir accès. Nous ne pouvions le
créer qu'à cet instant, car nous avions besoin de la texture.

\stopsubsection

\startsubsection[title={Le rendu}]
La dernière étape du rendu est le dessin sur la surface de rendu. Ceci se passe
dans {\tt MainPassNode} qui représente un nœud dans le graphe de rendu. Il
s'y trouve les différents objets qui ont été préparés à être rendus, et plus
particulièrement les tuiles. Il s'agira donc de modifier la partie responsable
de dessiner les tuiles vectorielles pour qu'elle puisse aussi le faire pour les
tuiles raster.

Aux étapes du {\tt QueueStage} et du {\tt PhaseSortStage}, maplibre-rs prépare
les différents éléments qui vont être rendus. {\tt MainPassNode} va parcourir ces
éléments et les rendre en utilisant ce qui a été défini dans le {\tt
render_commands}. Nous allons juste ajouter une manière de pouvoir différencier
entre les deux types de tuiles, qui ressemble à ceci:

\startRUST
for item in &state.tile_phase.items {
    if !item.0.is_raster() {
        DrawVectorTiles::render(state, item, &mut tracked_pass);
    } else {
        DrawRasterTiles::render(state, item, &mut tracked_pass);
    }
}
\stopRUST

Dans le {\tt render_commands} sont définies les commandes pour dessiner sur la
surface de rendu. L'ensemble des commandes pour dessiner les tuiles vectorielles
est défini ainsi:

\startRUST
pub type DrawVectorTiles = (
    SetVectorTilePipeline,
    SetVectorViewBindGroup<0>,
    DrawVectorTile,
);
\stopRUST

Les éléments qui constituent cet ensemble de rendu ont été définis au {\tt
RessourceStage}. Il y a juste {\tt DrawVectorTile} qui est spécifiquement décrit
dans le {\tt render_commands}. À l'intérieur nous pouvons y voir les différents
buffers qui vont être envoyés au Renderpass qui est lui responsable du rendu en
tant que tel. On y trouve:\blank[line]

\startdomain
\item des vertex buffers: buffers qui vont déterminer où se trouveront les
sommets sur la surface de rendu,
\item un index buffer qui indique comment la triangularisation va être effectuée
(l'étape de la tesselation pour les tuiles vectorielles),
\item la commande pour dessiner un sommet avec son index.
\stopdomain

Pour les tuiles raster, les deux buffers index et vertex sont plus simples, car
il s'agit de carrés. Il ne nous reste plus qu'à construire l'ensemble de
commandes pour le rendu de tuiles raster de manière équivalente à celle des
tuiles vectorielles et la partie du rendu est terminée.

\stopsubsection

\stopsection

\stopchapter