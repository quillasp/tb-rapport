\startchapter[title={Les rasters}]
Dans ce chapitre, nous allons voir les rasters, comment ils sont utilisés comme
données et comment les utiliser en tant que tuiles dans une application de rendu
de carte.

\startsection[title={Image raster}]
Une image raster est un autre terme pour les images pixellisées. Contrairement à
une image vectorielle qui est constituée d'un ensemble d'objets géométriques
(comme des courbes, droites, etc), une image pixellisée ou matricielle est un
tableau de points où chacun de ces points représentent une couleur qui lui est
propre.
\stopsection

\startsection[title={Donnée raster}]
Maintenant que nous savons ce qu'est un raster, nous nous intéresserons aux
données rasters, qui sont des rasters auxquelles chaque cellule (ou pixel)
contient une valeur qui représente une information (température, élévation de
terrain, etc).

\placefigure
  [fig:raster_dataset]
  {Une matrice où une cellule va contenir de l'information sur une zone géographique \from[raster-dataset]}
  {
    \externalfigure[raster_dataset.png][maxwidth=0.6\textwidth]
  }

Les données peuvent représenter différents phénomènes réels, selon les
informations qu'on veut avoir. Il peut s'agir d'images satellites ou aériennes,
ou des données calculées par, par exemple, des météorologues qui voudraient
représenter les précipitations, direction du vent, température moyenne de chaque
zone.

Voici quelques exemples de données raster que l'on peut trouver:\blank[line]

\startdomain
\item Sous la forme de carte, ces rasters sont souvent utilisés comme arrière-plan 
pour d'autres couches d'entités. On pourrait retrouver sous une couche de
carte routière par exemple, une orthophotographie\footnote[orthoimage-def]{Une
orthophotographie est une image aérienne ou satellite rectifiée géométriquement
pour faire en sorte d'avoir l'impression que l'image est plate, comme si elle
était prise à la verticale.} aérienne, satellite ou numérique.
\placefigure
  [fig:raster_route]
  {Exemple d'orthophotographie sous une carte routière \from[raster-route]}
  {
    \externalfigure[raster_route.png][maxwidth=0.6\textwidth]
  }
\item On peut également avoir des données qui changent constamment au fil du
temps sur des surfaces, comme les valeurs d'altitude à partir de la surface de
la Terre, mais également des précipitations, taux d'humidité ou densité de la
population.
\placefigure
  [fig:raster_altitude] {Exemple de données d'altitude, avec en vert une faible
  altitude et en rouge, rose et blanc une plus grande altitude
  \from[raster-altitude]} {
    \externalfigure[raster_altitude.png][maxwidth=0.6\textwidth]
  }
\item Encore un autre type de donnée raster sont les données thématiques qui
représentent des entités comme des données d'occupation du sol. L'idée est de
regrouper des données et leur attribuer des classes (eau, végétation, occupation
humaine).
\placefigure
  [fig:raster_theme]
  {Exemple de données thématiques, avec un jeu de données qui affiche l'occupation du sol \from[raster-theme]}
  {
    \externalfigure[raster_theme.png][maxwidth=0.8\textwidth]
  }
\stopdomain

On pourrait encore aller plus loin dans ce que sont les données raster, mais ce
n'est pas un des objectifs de ce travail. Nous avons d'ailleurs vu
le type de données raster qui va nous intéresser: les orthoimages satellites.
En l'état actuel, maplibre-rs permet déjà d'afficher une carte avec des tuiles
vectorielles. Il s'agira donc d'ajouter une couche avec ces données raster sous
la tuile vectorielle.
\stopsection

\startsection[title={Projection cartographique}]
La projection cartographique est un ensemble de méthode qui permet de
représenter la surface du globe terrestre sur une carte plane. Ici la projection
est une transformation mathématique qui permet de faire correspondre un point du
globe à un point de la surface.

Il existe différentes projections ayant chacune leur avantage et leur
inconvénients car il est impossible de représenter une sphère comme un rectangle
parfait sans déformations. Il y a quatre propriétés principales qui peuvent être
préservées ou déformées:\blank[line]

\startitemize[n][packed, broad, joinedup, nowhite, after, stopper=., distance=2em]
\starthead{L'aire}
Sur certaine carte on peut voir des parties du monde qui sont plus grandes que
dans la réalité. Dans la projection de Mercator, par exemple, le Groenland a
l'air aussi grand que l'Afrique alors qu'il est bien plus petit en réalité.

\placefigure
  [fig:mercator_projection]
  {Projection de Mercator \from[mercator-projection]}
  {
    \externalfigure[Mercator_projection_Square][width=0.3\textwidth]
  }

\stophead
\page
\starthead{La forme}
Suivant le type de projection, la forme n'est pas respectée. Par exemple, dans
la projection équidistante azimutale au pôle sud on peut voir le nord être tout
étiré.

\placefigure
  [fig:mercator_projection]
  {Projection équidistante azimutale \from[azimuthal-equidistant]}
  {
    \externalfigure[Azimuthal_equidistant_projection_south_SW][width=0.3\textwidth]
  }

\stophead
\starthead{La distance}
Les projections aphylactiques, comme la projection cylindrique équidistante,
conserve les distances sur les méridiens mais pas sur les parallèles. Le rapport
de la différence entre deux distances, ici entre (Madison-Buenos Aires,
Madison-Madrid), n'est pas conservée, comme on peut le voir dans l'exemple
ci-dessous.

\placefigure
  [fig:equirectangular]
  {Projection cylindrique équidistante \from[equirectangular-projection]}
  {
    \externalfigure[equirectangular][width=0.4\textwidth]
  }

\stophead
\page
\starthead{La direction}
Sur la projection de Mercator, la ligne droite entre Paris et New York n'est pas
le chemin le plus court. La route orthodromiques, qui est le chemin le plus
court, est en réalité une courbe sur ce type de projection.

\placefigure
  [fig:laxodrome-orthodrome]
  {Route laxodromiques en bleu et route orthodromique en rouge entre Paris et New-York \from[loxodrome-orthodrome]}
  {
    \externalfigure[Loxodromie][width=0.4\textwidth]
  }

\stophead
\stopdomain

La projection cartographique la plus connue est la cylindrique et plus
particulièrement celle de Mercator qui est utilisée en navigation, les routes
maritimes pouvant être facilement suivie au compas. Pour les cartes que le
retrouve sur le Web, c'est sa variante Web Mercator qui est utilisée.

\startsubsection[title={Tuiles rasters}]
Les tuiles rasters sont des tuiles dans un format d'image ({\emph .png} ou
{\emph .jpg}) placées les unes à côté des autres pour charger les parties de la
carte uniquement aperçue par l'utilisateur et non toute entière. Cela permet de
diviser les données raster en de plus petites parties qui sont plus faciles à
gérer.

\placefigure
  [fig:raster_tile_map]
  {Tuilage d'une carte du monde par rapport au niveau de zoom \from[raster-tile-map]}
  {
    \externalfigure[raster_tile_map.png][maxwidth=0.4\textwidth]
  }

\page

Il s'agit donc d'un assemblage d'images rectangulaires, avec différentes tailles
selon le niveau de zoom sur la carte, souvent arrangées dans un schéma
pyramidal.

\placefigure
  [fig:raster_tile_pyramid]
  {Schéma de tuiles pyramidal avec le niveau de zoom sur la carte \from[raster-tile-pyramid]}
  {
    \externalfigure[raster_tile_pyramid.jpg][maxwidth=0.5\textwidth]
  }

Un aspect à prendre en considération lors de l'utilisation de raster tiles,
c'est leur rendu. Une solution courante est de rendre le raster côté serveur
avant de le faire afficher chez l'utilisateur.
\stopsubsection
\stopsection
\stopchapter