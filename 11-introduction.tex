\startchapter[title={Introduction}]
Les moteurs de rendu de cartes jouent un rôle crucial dans une variété
d'environnements, tels que le Web, le bureau, les applications mobiles et les
applications embarquées. Par exemple, nous comptons sur eux pour nos trajets,
trouver les meilleurs restaurants ou localiser nos proches. Au fur et à mesure
qu'elles sont adoptées, de nouvelles applications apparaissent dans divers
domaines, comme l'urbanisme, les transports et même la surveillance des
pandémies.

Dans ce contexte, la portabilité du code est un problème courant. Par exemple,
Mapbox et Maplibre maintiennent toutes deux une base de code en JavaScript pour
le Web et en C++ pour les plateformes natives. Cela permet à leur moteur de
rendu Web de fonctionner sur les principaux navigateurs (grâce à WebGL), dans
les principaux environnements de bureau et mobiles, ainsi que dans les voitures
ou systèmes embarqués. Garantir que ces moteurs se comportent de manière
similaire et produisent les même résultats sur toutes les principales
plateformes est difficile, coûteux et ralentit la capacité des équipes de
développement d'innover.

C'est pourquoi ce travail vise à développer un moteur qui soit véritablement
portable et hautement performant grâce aux nouvelles technologies que sont Rust
et WebGPU.

Rust est un langage haut niveau conçu pour la sécurité et la haute performance.
Le projet a débuté chez Mozilla et est maintenant développé par la fondation
Rust. Son compilateur vise les architectures natives, ce qui permet aux
applications développées en Rust de pouvoir tourner en natif sur différentes
architectures (x86, ARM, …). Il peut également compiler en WebAssembly, ce qui
permet de les faire tourner sur le navigateur. Ceci afin de faire tourner, à
partir d'un même code source, les applications sur différentes plateformes
moyennant quelques modifications.

WebGPU est une API 3D bas niveau qui peut tourner sur différents backends
(Metal, DirectX, Vulkan, …) selon la plateforme et donne au développeur un accès
direct au GPU. Il est développé par le W3C avec la collaboration d'Apple,
Microsoft, Mozilla, Google et d'autres. Il est considéré comme le successeur de
WebGL/2. Contrairement à ses prédécesseurs, WebGPU n'est pas uniquement conçu
pour le Web, il implémente un header \inlineTEXT{webgpu.h} permettant de le
rendre cross-plateforme.

\startsection[title={Cahier des charges}]
Le moteur de rendu de carte sera une application pouvant tourner en natif et sur
le navigateur à partir d'un même code source. Le projet initial était de
concevoir un moteur de rendu de carte permettant de fonctionner autant sur le
Web grâce à WebGPU et en natif grâce aux bindings existant entre WebGPU et les
backends natif tels que Vulkan, OpenGL ou Metal. Mais suite à la publication du
projet open source maplibre-rs, les objectifs initiaux ont changé.

\startsubsection[title={Objectifs et livrables}]
Bien que maplibre-rs en soit encore à l'état de Proof of Concept (PoC), il
répond déjà aux objectifs du cahier des charges initial. Les nouveaux objectifs
seront donc des contributions au projet afin d'ajouter des fonctionnalités qui
ne sont pas encore prises en charge actuellement par le moteur. Deux en
particulier nous intéressent:\blank[line]

\startdomain
\item le rendu d'une couche satellite (raster tile),
\item le rendu d'un terrain 3D.
\stopdomain

Les livrables du travail seront les suivants:\blank[line]

\startdomain
\item un rapport intermédiaire détaillant les choix et raisons des technologies,
\item un rapport final détaillant la conception et l'implémentation du système,
\item un résumé publiable et un poster,
\item le code source de la contribution au projet maplibre-rs.
\stopdomain

Toute la documentation permettant de compiler et d'installer l'application sont
déjà disponibles dans le repository\footnote[maplire-rs-git]{\from[maplibre-rs]}
et les contributions ne devraient en théorie pas changer ces instructions.

\startsubsubsection[title={Objectifs fonctionnels}]
\startdomain
\item Le moteur doit pouvoir afficher une couche satellite sur la carte
vectorielle
\item Le moteur doit pouvoir afficher une couche de terrain 3D basée sur un
modèle d'élévation du terrain
\stopdomain
\stopsubsubsection

\startsubsubsection[title={Objectifs non-fonctionnels}]
\startdomain
\item Le moteur doit pouvoir afficher une carte satellite et un terrain 3D à 60
FPS
\item Les contributions doivent avoir une bonne couverture de test
\stopdomain
\stopsubsubsection
\stopsubsection

\startsubsection[title={Jalons}]
Voici les différents jalons du projet qui suivent le calendrier du travail de
bachelor.

\startsubsubsection[title={Jalon 1: semaine 15 (11-17 avril)}]
\startdomain
\item Rédaction du cahier des charges
\item Mise en place du rapport
\item Recherche sur les différents moteurs de carte existants
\item Recherche concernant les rendering backends en terme de portabilité
\stopdomain

Le 20 avril
2022\footnote[maplibre-rs-star-hist-l]{\from[maplibre-rs-star-hist]},
maplibre-rs devient public et mon superviseur et moi en parlons après le
stand-up meeting du 30 avril, pour choisir le chemin que va prendre le travail.
Il va donc changer d'objectifs pour ajouter des fonctionnalités au projet déjà
existant.
\stopsubsubsection

\startsubsubsection[title={Jalon 2: semaine 20 (16-20 mai)}]
\startdomain
\item Rédaction du rapport intermédiaire
\item Prise en charge des différentes technologies (telle que
wgpu\footnote[wgpu-git]{\from[wgpu]})
\item Description de la rastérisation
  \startdomain
  \item Qu'est-ce qu'un raster
  \item Qu'est-ce qu'un raster data
  \item Qu'est-ce qu'un raster tile
  \stopdomain
\stopdomain
\stopsubsubsection

%\framed[width=\textwidth, autowidth=force,align=right]
Ce jalon marque la fin des recherches de base sur le moteur de rendu de carte.
Comme il s'agit d'un travail exploratoire, toutes les variables ne sont pas
connues (langage relativement récent, WebGPU encore en cours d'élaboration, API
bas niveau nécessitant un temps d'apprentissage) ; de plus, l'implémentation
sera faite en suivant au mieux des sprints d'une à deux semaines, mais certaines
choses seront encore sujet à des changements.

\startsubsubsection[title={Jalon 3: semaine 26 (27 juin - 1 juillet)}]
\startdomain
\item Description de la solution
  \startdomain
  \item architecture
    \startdomain
    \item tiling
    \item raster tile
    \stopdomain
  \item charger les tuiles suivant la vue
  \item chargeur de tuiles fonctionnant sur le Web et natif
  \item voir comment les solutions existantes effectuent cette fonctionnalité
  \stopdomain
\item Mise en place de l'architecture des raster tiles
\item Chargement des raster tiles en fonction de la vue
\item Mise en place du rendu de terrain 3D
\item Rédaction du rapport final
\stopdomain
\stopsubsubsection

\startsubsubsection[title={Jalon 4: semaine 30 (25-29 juillet)}]
\startdomain
\item Finalisation du rapport final
\item Réalisation d'un résumé publiable et d'un poster
\item Objectifs non terminés
\stopdomain
\stopsubsubsection

Le travail se terminera le vendredi 29 juillet avec la remise du rapport. La
défense du travail aura lieu dans les semaines qui suivent.
\stopsubsection
\stopsection
\stopchapter