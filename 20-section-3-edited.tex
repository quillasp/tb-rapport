\startchapter[title={Conception}]
Maplibre-rs permet déjà le chargement des tuiles vectorielles et leur affichage
suivant les spécifications\footnote{Que l'on peut retrouver ici
\from[vector-tile-spec]}. L'objectif de ce travail est de modifier la
librairie pour qu'elle puisse prendre en charge les tuiles rasters. Pour ce
faire, nous avons quatre points à aborder:\blank[line]

\startdomain
\item effectuer les requêtes sur le service possédant les tuiles,
\item récupération et traitement de la tuile,
\item ajouter la tuile dans le dépôt de tuiles,
\item réaliser le rendu des tuiles au bon endroit sur la carte.
\stopdomain

Mais avant de parler de la manière d'incorporer ces nouvelles fonctionnalités
dans la librairie, nous allons passer en revue l'architecture existante, qui se
trouve à la page suivante.

\startsection[title={Fonctionnement de maplibre-rs}]
Pour commencer, il faut fournir des paramètres à {\tt MapBuilder} comme les
réglages de la fenêtre, le client HTTP et {\tt ScheduleMethod} qui
déterminera comment les étapes de rendu seront exécutées.

Puis {\tt MapBuilder} crée {\tt UnitializedMap} avec les paramètres fournis
et va à son tour initialiser {\tt InteractiveMapSchedule} qui va préparer
tout le pipeline de rendu ainsi que la fenêtre de l'application. Cela va
finir par créer une carte ({\tt Map}) prête à tourner.

Lors de l'initialisation, l'{\tt InteractiveMapSchedule} va créer les
différentes étapes {\tt Stage}s qui seront exécutés chacun 60 fois par seconde.
Les différents {\tt Stage}s sont les suivants:\blank[line]

\startitemize[n][packed, broad, joinedup, nowhite, after, stopper=., distance=2em]
\starthead{\tt RequestStage}
{\tt RequestStage} s'occupera d'effectuer les requêtes pour obtenir les
tuiles. C'est aussi lui qui mettra en place le pipeline qui fera la tesselation
des tuiles. C'est au travers du {\tt SharedThreadState} que les données des
tuiles seront échangées en l'utilisant à l'intérieur du {\tt
HeadedPipelineProcessor}.

{\tt HeadedPipelineProcessor} va créer le pipeline de tesselation, il enverra
la tuile à {\tt ParseTile} qui va transformer les données de la requête en une
tuile MVT\footnote{Pour Mapbox Vector Tile} et {\tt TessellateLayer} va faire la
tesselation de la tuile. 
\stophead
\starthead{\tt PopulateTileStoreStage}
Fonctionnant de concert avec {\tt RequestStage}, c'est lui qui se chargera de
déposer la tuile dans le {\tt TileRepository}. Il recevra la tuile traitée par le
{\tt SharedThreadState}. 
\stophead

\page

\starthead{\tt PrepareStage}
C'est à partir d'ici que le rendu des tuiles commence. {\tt PrepareStage} va
créer les différentes étapes qui permettront la mise en place des ressources pour
le rendu.
\stophead
\startitemize[n][packed, broad, joinedup, nowhite, after, stopper=., distance=2em]
\starthead{\tt ResourceStage}
Comme son nom l'indique, prépare les ressources qui seront envoyées au GPU comme
les pipelines des tuiles, les textures, le {\tt buffer_pool}, le {\tt
RenderState}, etc.
\stophead
\starthead{\tt ExtractStage}
Calcule quelle tuile sera dans la vue. Il le fait à partir du {\tt RenderState}
courant.
\stophead
\starthead{\tt UploadStage}
Charge toutes les données nécessaires au rendu dans le GPU.
\stophead
\stopitemize
\starthead{\tt QueueStage}
Prépare la file des différents éléments qui vont être rendus.
\stophead
\starthead{\tt PhaseSortStage}
Trie les éléments qui seront rendus selon l'ordre des couches.
\stophead
\starthead{\tt GraphRunnerStage}
Exécute le {\tt RenderGraph}, une technique de rendu inspirée de Bevy, un moteur
de jeu écrit en Rust et qui a aussi la possibilité de rendre avec wgpu. Mettra
aussi à jour le graphe de rendu avec tous ses nœuds pour rendre l'image entière.
\stophead
\starthead{\tt MainPass}
C'est dans {\tt MainPass} que se trouve la manière avec laquelle les éléments
seront rendus. Selon l'élément à rendre, il utilisera la bonne méthode de rendu.
\stophead
\stopitemize
\stopsection

\page \setuppapersize[extra]

\startsection[title={Architecture de la librairie}]
\midaligned{\externalfigure[maplibre_rs_schema.drawio.png][width=0.8\textwidth]}
\stopsection

\page \setuppapersize[A4]

\startsection[title={Modifications apportées à la librairie}]
Comme mentionné au début du chapitre, il faut effectuer trois principales
modifications à la librairie pour permettre d'effectuer le rendu des tuiles
rasters.

\startsubsection[title={Requête des tuiles}]
Comme indiqué précédemment, c'est à cette étape que maplibre-rs effectue les
requêtes pour les tuiles. Voici à quoi ressemble le schéma de
requête:\blank[halfline]

\placefigure
  [fig:source_before]
  {Procédé des requêtes de maplibre-rs}
  {
    \externalfigure[source.drawio.svg][width=0.9\textwidth]
  }

Ainsi {\tt RequestStage} passe par {\tt SourceClient} pour effectuer une requête
sur les coordonnées requises pour la vue courante. {\tt SourceClient} construira
l'URL avec les coordonnées reçues pour qu'à la fin, {\tt ReqwestHttpClient}
fasse une requête HTTP GET à l'URL. {\tt RequestStage} enverra les données de la
tuile au pipeline qui va par la suite procéder à la tesselation.

En l'état, maplibre-rs ne peut faire appel qu'à un seul type de source. Pour
permettre de faire des requêtes à au moins deux sources différentes, il nous
faut changer le procédé des requêtes. À la page suivante se trouve le schéma
modifié pour pouvoir mettre en place une nouvelle source.

\page

\placefigure
  [fig:source_after]
  {Procédé pour effectuer des requêtes à deux types sources différentes}
  {
    \externalfigure[source_after.drawio.png][width=0.9\textwidth]
  }

Pour le faire, il nous faudra faire un {\emph trait} {\tt Source} qui aura comme
méthode {\tt load()}. Les structures {\tt RasterSource} et {\tt
TessellateSource} implémenteront cette méthode. De plus, chacune d'elle devra
pouvoir construire l'URL à laquelle maplibre-rs fera les requêtes pour obtenir
le type de tuile désiré.

\stopsubsection

\startsubsection[title={Traitement de la tuile}]
Là aussi, maplibre-rs ne traite que les tuiles vectorielles. Voici un schéma
explicatif du procédé de traitement de la tuile:\blank[halfline]

\placefigure
  [fig:process_before]
  {Procédé du traitement d'une tuile vectorielle}
  {
    \externalfigure[process_tile.png][width=0.9\textwidth]
  }

Une fois les données de la requête précédente chargée, {\tt RequestStage} les
envoie au travers de {\tt SharedThreadState} à {\tt HeadedPipelineProcessor}
qui va créer le pipeline de tesselation qui consiste en deux étapes:\blank[line]

\startdomain
\item {\tt ParseTile} qui va parser les données de la requête en un tuile MVT.
\item {\tt TessellateLayer} qui va faire la tesselation des différentes couches
de la tuile vectorielle.
\stopdomain

Une fois le traitement terminé, la tuile est renvoyée à {\tt
HeadedPipelineProcessor} et la tuile sera chargée dans le dépôt de tuiles.

\page

Il nous faudra donc ajouter un pipeline de traitement de tuiles raster depuis
{\tt HeadedPipelineProcessor}. Voici ce que cela nous donnera:\blank[halfline]

\placefigure
  [fig:process_after]
  {Schéma modifié du traitement d'une tuile}
  {
    \externalfigure[process_tile_after.svg][width=0.9\textwidth]
  }

{\tt HeadedPipelineProcessor} créera un deuxième pipeline de traitement pour les
tuiles raster. En plus de cela, nous allons découpler l'étape {\tt
TessellateLayer} qui fait la tesselation et envoie la tuile pour le chargement
dans le dépôt en deux étapes: {\tt TessellateLayer} qui fera toujours la
tesselation et {\tt TilePipeline} qui enverra la tuile traitée à {\tt
HeadedPipelineProcessor}. Tout ceci pour pouvoir y rattacher {\tt RasterLayer}
qui recevra les données de la tuile raster, effectuera un traitement (qui n'est
pas encore nécessaire dans le cadre de ce travail), puis enverra aussi la tuile
à {\tt TilePipeline}.

\stopsubsection

\page

\startsubsection[title={Rendu de la tuile}]
Nous avons vu au point \{architecture de maplibre-rs\} que le rendu de la
tuile était séparé en six étapes; voici un schéma mettant les étapes du rendu en
évidence:\blank[halfline]

\placefigure
  [fig:process_after]
  {Rendu d'une tuile dans maplibre-rs}
  {
    \externalfigure[tile_render.svg][width=0.9\textwidth]
  }

Il ne sera pas nécessaire d'intervenir sur chacune de ces étapes pour ajouter le
rendu des tuiles raster. Les étapes concernées sont:\blank[line]

\startdomain
\item {\tt ResourceStage} pour préparer les éléments nécessaires au rendu de la
tuile comme le pipeline de rendu,
\item {\tt UploadStage} pour préparer la mémoire dans le GPU comme les sommets
(vertex) et les textures,
\item {\tt MainPass} car les données seront déjà préparées pour les étapes qui
viennent avant. C'est ici que l'on dessinera les éléments à rendre.
\stopdomain

Il y a encore un autre endroit qui aura besoin d'être modifié, mais qui n'est pas
indiqué dans le schéma: il s'agit des commandes de rendu ({\tt render_commands})
qui contiendra les instructions pour dessiner sur la surface.

\page

\startsubsubsection[title={Rendu d'une texture}]
Mais avant de voir comment nous allons modifier ces étapes, intéressons-nous sur
la manière de pouvoir rendre une texture avec wgpu:\blank[line]

\startdomain
\item nous aurons besoin de décoder les données qui se trouvent dans le dépôt de
tuiles en une image, puis en un vecteur de 8-bit RGBA bytes qui représentent les
pixels,
\item avec l'image, nous pourrons obtenir ses dimensions,
\item ensuite nous construirons la texture,
\item nous écrirons la texture dans le GPU avec les pixels
et les dimensions obtenues.
\item il nous faudra aussi créer la vue de la texture,
\item et pour terminer nous aurons besoin de créer un sampler pour la texture.
\stopdomain

Voilà les différents éléments dont nous aurons besoin pour pouvoir afficher une
texture grâce au GPU.

Nous devons aussi parler de la manière avec laquelle nous pouvons représenter un
carré. Le GPU n'a que trois primitives: les points, les lignes et les triangles.
Il nous faudra donc décomposer notre carré en deux triangles. Voici un schéma
explicatif de la manière avec laquelle on procédera:\blank[halfline]

\placefigure
  [fig:square_texture]
  {Séparation d'un carré en deux triangles}
  {
    \externalfigure[square_texture.png][width=0.4\textwidth]
  }

Pour un carré abcd, nous définirons deux triangles Δabd et Δdbc, chacun arrangé
dans l'ordre inverse des aiguilles d'une montre.

\page

Nous devons aussi parler des coordonnées de la texture. La plupart du temps,
elle possède des dimensions (hauteur et largeur) en puissance de deux (64, 128,
256, etc). Pour passer des coordonnées de l'image à celles de la texture, nous
allons les normaliser afin d'obtenir des intervalles entre \m{[0,
1]}.

\placefigure
  [fig:texture_coords]
  {Représentation des coordonnées d'une texture}
  {
    \externalfigure[texture_coords.svg][width=0.4\textwidth]
  }

Puis nous ferons correspondre chacun des sommets de la surface sur laquelle nous
rendons la texture avec ces coordonnées. La couleur de chaque sommet sera
déterminée par le sampler que nous avons défini avant.

\stopsubsubsection

\startsubsubsection[title={Préparation pour le rendu}]
Comme nous l'avons dit, il nous faudra intervenir sur deux étapes du {\tt
PrepareStage}, ceci afin de mettre en place les ressources nécessaires à la carte
graphique pour qu'elle puisse rendre un raster comme une texture.

Regardons donc de plus près ce qu'il nous faut dans les deux étapes concernées.

\startsubsubsubsection[title={Préparation des ressources}]
Dans cette étape, les éléments telles que la surface de rendu, la texture de
profondeur, le buffer pool et les différents pipelines de rendu sont préparés.
Comme tout est mis en place pour les tuiles vectorielles, nous aurons à rajouter
les ressources pour le rendu de texture à partir d'images.

Nous devrons donc ajouter une structure pour les sommets qui représenteront les
quatres coins de la texture, une autre pour le format, écrire les shaders et
nous pourrons construire le pipeline de rendu.
\stopsubsubsubsection

\startsubsubsubsection[title={Chargement dans la mémoire GPU}]
Ici, nous préparerons les différents éléments qui seront par la suite chargés
dans le GPU. Nous commencerons par préparer la taille des buffers avant d'écrire
les informations que l'on veut dedans comme les coordonnées de la tuile. Dans
cette même partie, nous créerons la texture à partir de l'image en suivant le
procédé que nous avons décrit et la chargerons à son tour dans le GPU.

La partie la plus délicate va être de correctement définir les coordonnées des
sommets de la tuile, car nous ne voulons pas simplement en afficher une sur la
surface de rendu, mais sur une grille carrée qui contiendra les différentes
tuiles. Et sur chacune de ces tuiles, nous devons aussi désigner les coordonnées de 
chaque texture que nous voulons afficher dessus. Et bien évidemment, il faudra
aussi préparer les indices qui désigneront les triangles pour chaque tuile.

\stopsubsubsubsection

\stopsubsubsection

\stopsubsection

Avec tout ceci, nous devrions en avoir fini avec la partie de conception, car
comme indiqué au début du chapitre, le projet a déjà une structure, donc nous
n'avons pas en à mettre en place une nouvelle. Toutefois nous devrons la modifier de
manière adéquate afin de pouvoir parvenir à cet objectif de rendre des tuiles
raster sur une carte.
\stopsection

\stopchapter